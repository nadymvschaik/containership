using System;
using Xunit;
using ContainerShip;
using ContainerShip.Core;
using System.Collections.Generic;
namespace ContainerShip.Tests
{
    public class ApplicationTest
    {
        /// <summary>
        /// Escapes the .net bin path
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string escapeFromBinPath(string fileName)
        {
            return "../../../" + fileName;
        }

        /// <summary>
        /// Test whether the MaxWeight of a ship is read correctly from input
        /// </summary>
        [Fact]
        public void testIfShipMaxWeightIsReadCorrectly()
        {
            //act   
            Application application = new Application();
            application.readinput(this.escapeFromBinPath("input.txt"));

            Ship ship = application.Ship;

            //assert
            Assert.True(ship.MaxWeight == 500000);
        }

        /// <summary>
        /// Test whether the ShipWidth of a ship is read correctly from input
        /// </summary>
        [Fact]
        public void testIfShipShipWidthIsReadCorrectly()
        {
            //act   
            Application application = new Application();
            application.readinput(this.escapeFromBinPath("input.txt"));

            Ship ship = application.Ship;

            //assert
            Assert.True(ship.ShipWidth == 5);
        }

        /// <summary>
        /// Test whether the ShipHeight of a ship is read correctly from input
        /// </summary>
        [Fact]
        public void testIfShipShipHeightIsReadCorrectly()
        {
            //act   
            Application application = new Application();
            application.readinput(this.escapeFromBinPath("input.txt"));

            Ship ship = application.Ship;

            //assert
            Assert.True(ship.ShipHeight == 3);
        }

        /// <summary>
        /// Test whether the ShipLength of a ship is read correctly from input
        /// </summary>
        [Fact]
        public void testIfShipShipLengthIsReadCorrectly()
        {
            //act   
            Application application = new Application();
            application.readinput(this.escapeFromBinPath("input.txt"));

            Ship ship = application.Ship;

            //assert
            Assert.True(ship.ShipLength == 7);
        }

        /// <summary>
        /// Test whether the ShipWidth of a ship is not 0
        /// </summary>
        [Fact]
        public void testIfShipShipWidthIsNotZero()
        {
            Application application = new Application();
            Ship ship = application.Ship;
            Assert.Throws<NotSupportedException>(() => application.readinput(this.escapeFromBinPath("shipWeightIsZero.txt")));
        }

        /// <summary>
        /// Test whether the ShipLength of a ship is not 0
        /// </summary>
        [Fact]
        public void testIfShipShipLengthIsNotZero()
        {
            Application application = new Application();
            Ship ship = application.Ship;
            Assert.Throws<NotSupportedException>(() => application.readinput(this.escapeFromBinPath("shipLengthIsZero.txt")));
        }

        /// <summary>
        /// Test whether the ShipHeight of a ship is not 0
        /// </summary>
        [Fact]
        public void testIfShipShipHeightIsNotZero()
        {
            Application application = new Application();
            Ship ship = application.Ship;
            Assert.Throws<NotSupportedException>(() => application.readinput(this.escapeFromBinPath("shipHeightIsZero.txt")));
        }

        /// <summary>
        /// Test whether the Ship is not empty
        /// </summary>
        [Fact]
        public void testIfShipIsNotEmpty()
        {
            Application application = new Application();
            Ship ship = application.Ship;
            Assert.Throws<NotSupportedException>(() => application.readinput(this.escapeFromBinPath("input.txt")));
        }

        /// <summary>
        /// Test whether the ShipHeight of a ship is not 0
        /// </summary>
        [Fact]
        public void testIfShipShipMaxWeightIsNotZero()
        {
            Application application = new Application();
            Ship ship = application.Ship;
            Assert.Throws<NotSupportedException>(() => application.readinput(this.escapeFromBinPath("shipMaxWeightIsZero.txt")));
        }
    }
}
