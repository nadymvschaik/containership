using Xunit;
using ContainerShip;
using ContainerShip.Core;
using System.Collections.Generic;

namespace ContainerShip.Tests
{
    public class ContainerShipTest
    {
        /// <summary>
        /// Escapes the .net bin path
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string escapeFromBinPath(string fileName)
        {
            return "../../../" + fileName;
        }
    }
}