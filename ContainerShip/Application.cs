﻿using System;
using ContainerShip.Core;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace ContainerShip
{
    public class Application
    {
        private Ship ship;
        public Ship Ship {
            get { return ship; }
            set { ship = value; }
        }


        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        /// <summary>
        /// reads the input from a file and write is to ship
        /// </summary>
        /// <param name="filename"></param>
        public void readinput(string filename)
        {
            string[] inputfile = File.ReadAllLines(filename);
            bool firstRowRead = false;
            ship = new Ship();

            foreach(string input in inputfile)
            {
                if(!firstRowRead)
                {
                    parseShipInput (input);
                     firstRowRead = true;
                }
                else
                {
                    parseShipInput(input);
                }
            }
        }

        /// <summary>
        /// Parses the ship information from the input file
        /// </summary>
        /// <param name="input"></param>
        public void parseShipInput(string input){
            string[] shipProperties = input.Split(';');
            ship.MaxWeight = Int32.Parse(shipProperties[0]);

            if(ship.MaxWeight <= 0)
            {
                throw new NotSupportedException("ship MaxWeight cannot be zero or less");
            }

            ship.ShipWidth =  Int32.Parse(shipProperties[1]);
            if(ship.ShipWidth <= 0)
            {
                throw new NotSupportedException("shipWidth cannot be zero or less");
            }

            ship.ShipHeight = Int32.Parse(shipProperties[2]);
            if(ship.ShipHeight <= 0)
            {
                throw new NotSupportedException("shipHeight cannot be zero or less");
            }

            ship.ShipLength = Int32.Parse(shipProperties[3]);
            if(ship.ShipLength <= 0)
            {
                throw new NotSupportedException("shipLength cannot be zero or less");
            }

        /// <summary>
        /// Parses container information from the input
        /// </summary>
        /// <param name="input"></param>          

        }
        public void parseContainerInput(string input)
        {
            string[] inputContainers = input.Split(":");

            switch(inputContainers[1].ToLower()) {
                case "v":
                    //add this container to the list ValuableContainers
                    break;
                case "c":   
                    //add this container to the list CooledContainers
                    break;
                case "cv":
                    //add this container to the list ValuableCooledContainers
                    break;
                case "n":
                    //add this container the the list Containers
                    break;
            }
        }
    }
}
