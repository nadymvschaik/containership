﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ContainerShip.Core
{
    public class Ship
    {
        private int maxWeight;
        private int minCapacity;
        private int shipWidth;
        private int shipHeight;
        private int shipLength;

        private List<Row> rows;

        /// <summary>
        /// Ship constructor
        /// </summary>
        public Ship() {
            rows = new List<Row>();
        }

        public int MaxWeight
        {
            get { return maxWeight; }
            set { maxWeight = value; }
        }

        public int MinCapacity
        {
            get { return minCapacity; }
            set { minCapacity = value; }
        }

        public int ShipWidth
        {
            get { return shipWidth; }
            set { shipWidth = value; }
        }

        public int ShipHeight
        {
            get { return shipHeight; }
            set { shipHeight = value; }
        }

        public int ShipLength
        {
            get { return shipHeight; }
            set { shipHeight = value; }
        }

        public List<Row> Rows
        {
            get { return rows; }
            set { rows = value; }
        }


        /// <summary>
        /// Checks the preconditions to see whether placing these containers is theoratically possible
        /// </summary>
        /// <returns>bool</returns>
        public bool checkPreConditions()
        {
            throw new NotImplementedException();
        }
    }
}
